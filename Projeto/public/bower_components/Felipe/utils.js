var graphs = [];
//Define o id
var id = 0;

//Teste da função de adicionar informações
function execute_addData (my_id, s_eixo_x,s_eixo_y) {
$.post("/db_2",
        function (data)
          {
          //console.log(data);
          var eixo_x = [];
          var eixo_y = [];
          for (var i in data) {
          eixo_x.push(data[i][s_eixo_x]);
          eixo_y.push(data[i][s_eixo_y]);
          };
          //console.log(my_id);
          addData(graphs[my_id], s_eixo_y, '#ff0000', eixo_y);
        });
};

function addData(chart, label, color, data) {
  chart.data.datasets.push({
    label: label,
    fill: false,
    backgroundColor: color,
    data: data
  });
  chart.update();
}

//Funcao para adicionar grafico de barras
function add_chart(type) {

  //Adiciona o canvas
  var div = document.createElement("div");  // Create with DOM
  div.id = "idDivChart_" + id.toString();
  $("#conteudo").append(div); 

  var this_id = id.toString();
  console.log('id: ' + this_id);
  var button = document.createElement("button");  // Create with DOM
  button.id = "idButton_" + id.toString();
  button.class = "btn btn-primary btn-lg";
  button.innerHTML = "Config";
  button.addEventListener('click', function() {

  //Preenche o modal com o campo id
  $("#exampleModal_data").find('#id').val(id-1);
  //Comando para abrir o modal
  $("#exampleModal_data").modal();
  //execute_addData(this_id,'Destino','tempo_2');
  }, false);

  var graf = document.createElement("canvas");  // Create with DOM
  graf.id = "myChart_" + id.toString();
  
  $("#idDivChart_" + id.toString()).append(button);      // Append the new elements
  $("#idDivChart_" + id.toString()).append(graf);      // Append the new elements

  //Adicona o script que faz funcionar
  var script = document.createElement("script");
  script.innerHTML = `
  $(document).ready(function () {
    showGraph();
    });
    function showGraph()
    {
      $("#myChart_` + id.toString() + `").on('dblclick', 'tr', function () {
      //var modal = $("#exampleModal");
      alert("ola");
      //$("#exampleModal").modal();
      });
      {
        $.post("/db_2",
        function (data)
          {
          console.log(data);
          var name = [];
          var marks = [];
          for (var i in data) {
          name.push(data[i].Destino);
          marks.push(data[i].tempo);
          }
        var chartdata = {
        labels: name,
        datasets: [
          {
          label: 'Tempo (s)',
          fill: false,
          backgroundColor: '#49e2ff',
          borderColor: '#46d5f1',
          hoverBackgroundColor: '#CCCCCC',
          hoverBorderColor: '#666666',
          data: marks
          }
        ]
        };
        console.log("#myChart_`+ id.toString() + `");
        var graphTarget = $("#myChart_`+ id.toString() + `");
        var barGraph = new Chart(graphTarget, {
        type: '` + type.toString() + `',
        data: chartdata
        });
        graphs.push(barGraph);
        });
      }
    }
  `;

  //Incremementa o id
  id = id + 1;

  document.head.appendChild(script);
  }


//Preenche combobox
function preenche_combo (comboId, options) {
  var select = document.getElementById(comboId);

  for(var i = 0; i < options.length; i++) {
      var opt = options[i];
      var el = document.createElement("option");
      el.textContent = opt;
      el.value = opt;
      select.appendChild(el);
  }
};