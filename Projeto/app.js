//Carrega os modulos utilizados
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser')
const mysql = require('mysql');
const sql = require('mssql');
const {spawn} = require('child_process');

//Inicia o aplicativo
const app = express();

//Para pegar informacoes das paginas HTML
app.use(bodyParser.urlencoded({ extended: false}))
app.use(bodyParser.json())

//Define a pasta com recursos publicos
app.use(express.static(path.join(__dirname, 'public')));

//Carrega a pasta views
app.set('views',path.join(__dirname,'views') );
app.set('view engine', 'pug');

//Define qual a porta do servidor
const port = 3000;

//Route: Graficos
app.get('/', function(req, res){
    res.render('login', {});
});

//Add route
app.get('/articles/add', function(req, res){
	res.render('add_article', {
		title: 'Add Article'
	});
});

//Route: Testes
app.get('/tabela', function(req, res){
	res.render('test_view_4', {});
});

//Route: Graficos
app.get('/graficos', function(req, res){
	res.render('graficos_2', {});
});


//Add submit POST
app.post('/articles/add', function(req,res) {
	console.log(req.body.title);
	return;
});

//Route: Json from database
app.get('/db', function (req, res) {
   
    var sql = require("mssql");

    // config for your database
    var config = {
        user: 'sa',
        password: 'logix16',
        server: 'DESKTOP-TOVMU6G', 
    	options: {
        	encrypt: false, // Use this if you're on Windows Azure
        	instanceName: 'SQLEXPRESS'
    	},
        database: 'Caixas' 
    };

    // connect to your database
    sql.connect(config, function (err) {
    
        if (err) console.log(err);

        // create Request object
        var request = new sql.Request();
           
        // query to the database and get the records
        request.query('select TOP(10) * from Caixas', function (err, recordset) {
            
            if (err) console.log(err)

            recordset.data = recordset.recordset;
			delete recordset.recordset;
			delete recordset.recordsets;
			delete recordset.output;
			delete recordset.rowsAffected;

            // send records as a response
            res.send(recordset);

            // send records as a response
            //res.send(recordset.recordsets);
            
        });
    });
});

//Route: Json from database
app.post('/db_2', function (req, res) {
   
    var sql = require("mssql");

    // config for your database
    var config = {
        user: 'sa',
        password: 'logix16',
        server: 'DESKTOP-TOVMU6G', 
        options: {
            encrypt: false, // Use this if you're on Windows Azure
            instanceName: 'SQLEXPRESS'
        },
        database: 'Caixas' 
    };

    // connect to your database
    sql.connect(config, function (err) {
    
        if (err) console.log(err);

        // create Request object
        var request = new sql.Request();
           
        // query to the database and get the records
        request.query('SELECT TOP(10) [Destino],sum([Tempo]) as tempo,sum([Tempo])/2 as tempo_2 FROM [Caixas].[dbo].[Tempo] group by [Destino] order by tempo asc', function (err, recordset) {
            
            if (err) console.log(err)

            recordset.data = recordset.recordset;
            delete recordset.recordset;
            delete recordset.recordsets;
            delete recordset.output;
            delete recordset.rowsAffected;

            // send records as a response
            res.send(recordset.data);
            
        });
    });
});

//Route: Json from python
app.get('/python', (req, res) => {
var largeDataSet = [];
 // spawn new child process to call the python script
 const python = spawn('python', ['script3.py']);
 // collect data from script
 python.stdout.on('data', function (data) {
  console.log('Pipe data from python script ...');
  largeDataSet.push(data);
 });
 // in close event we are sure that stream is from child process is closed
 python.on('close', (code) => {
 console.log(`child process close all stdio with code ${code}`);
 // send data to browser
 res.send(largeDataSet.join(""))
 });
 
})

//Start do server
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));